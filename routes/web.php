<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->group(['prefix' => 'user'], function () use ($router) {
        $router->post('register', 'UserApiController@register');
        $router->post('sign-in', 'UserApiController@login');
        $router->post('recover-password', 'UserApiController@passreset');

        $router->group(['middleware' => 'auth'], function () use ($router) {
            $router->patch('recover-password', function () use ($router) {
                return $router->app->version(); //@todo
            });
            $router->get('companies', 'CompanyApiController@getCompaniesByUser');

            $router->post('companies', 'CompanyApiController@storeCompanyByUser');

        });
    });
});
