<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 9) as $index) {
            DB::table('companies')->insert([
                'title' => $faker->company,
                'user_id' => $faker->randomDigitNot(0),
                'description' => $faker->text($maxNbChars = 200)  ,
                'phone' => $faker->phoneNumber ,
            ]);
        }
    }
}
