# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://img.shields.io/packagist/dt/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://img.shields.io/packagist/v/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://img.shields.io/packagist/l/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Application Run

#1
`git clone`

#2
`composer install`

#3
`php artisan migrate`
`php artisan db:seed`

#4
`php -S localhost:8000 -t public` to run server

## API doc

api list
|number |method |url                        |format                                                                                                                           |
|:-----:|:-----:|:-------------------------:|:-------------------------------------------------------------------------------------------------------------------------------:|
|1      |POST   |/api/user/register         |{ "first_name": "Annabelle", "last_name": "Kek","email": "mailmail@gmail.com", "password": "123456789", "password": "0992881488"}|
|2      |POST   |/api/user/sign-in          |{ "email": "mailmail@gmail.com","password": "159159159"}                                                                         |
|3      |POST   |/api/user/recover-password |                                                                                                                                 |
|4      |GET    |/api/user/companies        |return companies by user {"title": "blablabla Group", "description": "bla bla","phone": "987-123-1212"}                          |
|5      |POST   |/api/user/companies        |{"title": "blablabla Group", "description": "bla bla","phone": "987-123-1212"}                                                   |

Auth with bearer token in headers
