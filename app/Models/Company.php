<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;


class Company extends Model
{
    protected $fillable = [
        'title', 'description', 'phone',
    ];

    /**
     * @param $validated
     * @param User $user
     */
    public function store($validated, User $user)
    {
        $current_user = $user->getUser();
        DB::table('companies')->insert([
                'user_id' => $current_user['id'],
                'title' => $validated['title'],
                'description' => $validated['description'],
                'phone' => $validated['phone']
            ]);
    }

    public function companies(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id');
    }

}
