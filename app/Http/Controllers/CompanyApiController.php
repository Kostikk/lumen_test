<?php


namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\JWTAuth;

class CompanyApiController extends Controller
{
    /**
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function getCompaniesByUser(User $user): JsonResponse
    {
        $data = $user->companies();
        //dd($data);
        return response()->json(['companies' => $data]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @param Company $company
     * @return JsonResponse
     */
    public function storeCompanyByUser(Request $request, User $user, Company $company): JsonResponse
    {
        try {
            $validated = $this->validate($request, [
                'title' => 'required|string',
                'description' => 'required|string',
                'phone' => 'required|string',
            ]);
            $company->store($request, $user);

            return response()->json(['companies' => 'added'],201);
        }
        catch (\Exception $e)
        {
            return response()->json(['error' => $e],400);
        }

    }
}
